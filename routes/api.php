<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'LoginController@login');

Route::post('/adicionar-produtos', 'ProdutoController@adicionarProdutos');

Route::post('/remover-produtos', 'ProdutoController@removerProdutos');

Route::post('/adicionar-estoque', function () {
    return 'Retorno temporário';
});

Route::post('/relatorio', 'ProdutoController@relatorio');

Route::get('/categoria', 'CategoriaController@lista');

Route::get('/produto-sku', 'ProdutoController@lista');

Route::get('/cliente', 'ClienteController@lista');

