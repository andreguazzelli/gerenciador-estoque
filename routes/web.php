<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//view('welcome');

Route::get('/login', function () {
    return 'Retorno temporário';
});

Route::get('/adicionar-produtos', function () {
    return 'Retorno temporário';
});

Route::get('/remover-produtos', function () {
    return 'Retorno temporário';
});

Route::get('/adicionar-estoque', function () {
    return 'Retorno temporário';
});

Route::get('/relatorio', function () {
    return 'Retorno temporário';
});

