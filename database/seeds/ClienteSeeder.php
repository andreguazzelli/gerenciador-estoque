<?php

use Illuminate\Database\Seeder;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'cliente';

    	$insert = array([
            'nome' => 'Paulo da Soares da Silva',
            'cpf' => '63014233137',
            'created_at' => date('Y-m-d h:i:s'),
        ],[
            'nome' => 'Maria Machado',
            'cpf' => '22830781996',
            'created_at' => date('Y-m-d h:i:s'),
        ],[
            'nome' => 'Roberto Carlos Souza Filho',
            'cpf' => '85518426410',
            'created_at' => date('Y-m-d h:i:s'),
        ],[
            'nome' => 'Leonardo Fagundes Ferreira',
            'cpf' => '51874574626',
            'created_at' => date('Y-m-d h:i:s'),
        ],[
            'nome' => 'Carla Garcia Pereira',
            'cpf' => '62828622304',
            'created_at' => date('Y-m-d h:i:s'),
        ],[
            'nome' => 'Antônio Ubirajara dos Reis',
            'cpf' => '44976224104',
            'created_at' => date('Y-m-d h:i:s'),
        ]);

    	DB::table($table)->truncate();

        DB::table($table)->insert($insert);
    }
}
