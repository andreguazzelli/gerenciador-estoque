<?php

use Illuminate\Database\Seeder;
use App\Resources\Tricks;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//php artisan make:seeder UsuarioSeeder
    	$table = 'usuario';

    	$insert = array([
            'login' => 'admin',
            'senha' => Tricks::gerarSenha('123456'),
            'ativo' => true,
            'created_at' => date('Y-m-d h:i:s'),
        ]);

    	DB::table($table)->truncate();

        DB::table($table)->insert($insert);
    }


}
