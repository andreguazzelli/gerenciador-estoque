<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutoTransacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produto_transacao', function (Blueprint $table) {
            $table->id();
            $table->integer('id_produto_estoque');
            $table->enum('tipo_transacao', ['adicionado', 'removido']);
            $table->enum('tipo_usuario', ['sistema', 'api']);
            $table->integer('id_cliente')->nullable();
            $table->integer('quantidade');
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produto_transacao');
    }
}
