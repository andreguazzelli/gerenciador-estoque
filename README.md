# GERENCIADOR DE ESTOQUE #

## Tabelas existentes ##

* produto
* produto_categoria
* produto_estoque
* produto_preco
* produto_transacao
* produto_sku
* categoria
* usuario
* token
* cliente

## Endpoints ##

### API ###

* [POST]	/api/login
* [POST]	/api/adicionar-produtos
* [POST]	/api/remover-produtos
* [POST]	/api/adicionar-estoque
* [POST]	/api/relatorio
* [GET]		/api/categoria
* [GET]		/api/produto-sku
* [GET]		/api/cliente

### WEB ###

* [GET]		/login
* [GET]		/adicionar-produtos
* [GET]		/remover-produtos
* [GET]		/adicionar-estoque
* [GET]		/relatorio


## Regras de negócio ##

* Uma tela para adicionar produtos ao estoque.

* Uma tela para dar baixa em produtos que serão enviados aos clientes.

* Um relatório de produtos movimentados por dia com: Quantos e quais produtos foram adicionados ao estoque; Quantos e quais produtos foram removidos do estoque; Se a adição/remoção foi feita via sistema ou via API; e Aviso de estoque baixo quando um produto possuir menos de 100 unidades no estoque. 

* O sistema deverá estar protegido por um sistema de login.

* Validação de quantidade de produtos: Não poderá remover produtos caso não possua a quantidade desejada. 

* Não poderão ser cadastrados produtos com SKU duplicados (código para o produto). 

* Um sistema bem organizado que siga os padrões de desenvolvimento.

* Alguma documentação de método em caso de necessidade.

* Commits com uma boa descrição sobre o que foi implementado em cada alteração.

* Alguns testes unitários. 


## Versões usadas no desenvolvimento ##

* PHP Version: 7.3.9
* MYSQL Version: MariaDB 10.4.6 


## Como instalar e migrar dados ##


````
$ php artisan migrate
$ php artisan db:seed
````

## Exemplos de parâmetros ##

* [POST]	/api/login
````
{
	"login": "admin",
	"senha": "123456"
}
````

* [POST]	/api/adicionar-produtos
````
{
	"token": "YjVjNGJiYTU2ZmU1OWFkZGJjMDhiNDA0MTBiYzEzNTVhZG1pbjg2Mg==",
	"nome": "Bicicleta",
	"descricao": "Descricao",
	"codigo_barras": "1211A",
	"categorias": [1,2],
	"sku": "BIDE1254",
	"quantidade": 110,
	"preco_unitario_compra": 240.45,
	"preco_sugerido_venda": 360.15
}
````

* [POST]	/api/remover-produtos
````
{
	"token": "YjVjNGJiYTU2ZmU1OWFkZGJjMDhiNDA0MTBiYzEzNTVhZG1pbjg2Mg==",
	"sku": "BIDE1254",
	"quantidade": 1,
	"preco_unitario": 360.15,
	"id_cliente": 3
}
````

* [POST]	/api/relatorio
````
{
	"token": "YjVjNGJiYTU2ZmU1OWFkZGJjMDhiNDA0MTBiYzEzNTVhZG1pbjg2Mg==",
	"sku": "2020-04-20"
}
````
