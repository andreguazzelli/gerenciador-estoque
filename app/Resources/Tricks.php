<?php

namespace App\Resources;

class Tricks
{

	public static function gerarSenha($senha){
		$return = md5($senha);
		return $return;
	}

	public static function geraToken($login){
		$token = base64_encode(md5(date('Y-m-d h:i:s')). $login . rand(0,5000));
		return $token;
	}

	public static function validacaoEntrada($params, $params_necessarios){

		$return = [
			"message" => "",
			"validate" => true
		];

		foreach ($params_necessarios as $key => $value) {
			if(!array_key_exists($value, $params) || isset($params[$value]) != true) {
				$return["message"] = "Parâmetro '$value' é necessário";
				$return["validate"] = false;
			    return $return;
			}
		}

		return $return;
		
	}

	public static function headerJson(){
		return array(
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'utf-8'
        );
	}




	
}