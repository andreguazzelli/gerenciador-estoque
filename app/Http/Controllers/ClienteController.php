<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resources\Tricks;
use App\Model\Cliente;

class ClienteController extends Controller
{
    public function lista(){

    	$cliente = Cliente::select('id','nome')->orderBy('nome')->get();

    	return response()->json($cliente, 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);

    }
}
