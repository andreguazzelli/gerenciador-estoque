<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resources\Tricks;
use App\Model\Categoria;

class CategoriaController extends Controller
{
    public function lista(){

    	$categoria = Categoria::select('id','nome')->orderBy('nome')->get();

    	return response()->json($categoria, 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);

    }
}
