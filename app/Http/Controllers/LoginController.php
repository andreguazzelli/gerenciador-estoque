<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resources\Tricks;
use App\Model\Usuario;
use App\Model\Token;

class LoginController extends Controller
{
    public function login(Request $request){
    	$input = $request->input();

    	//verifica existência dos parâmetros necessários
    	$params_entrada = ["login","senha"];
    	$validaParams = Tricks::validacaoEntrada($input,$params_entrada);
    	if($validaParams['validate'] == false){

    		return response()->json($validaParams, 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
    	}

    	$login = $input['login'];
    	$senha = $input['senha'];

    	//verifica se login e senha estão corretos
    	$verificaLogin = Usuario::where('login', $login)->where('senha', Tricks::gerarSenha($senha))->where('ativo', true)->first();

    	if(!$verificaLogin){

    		return response()->json(["message"=>"Login ou senha inválido. Ou então, Usuário inativo","token"=>""], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
    	}

    	$token = Tricks::geraToken($login);

    	$gravaToken = Token::insert(['hash_token' => $token, 'id_usuario' => $verificaLogin->id, 'created_at' => date('Y-m-d h:i:s')]);


    	return response()->json(["message"=>"Login realizado","token"=>$token], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
    }
}
