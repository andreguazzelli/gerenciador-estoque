<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resources\Tricks;
use App\Model\Cliente;
use App\Model\Produto;
use App\Model\ProdutoCategoria;
use App\Model\ProdutoEstoque;
use App\Model\ProdutoPreco;
use App\Model\ProdutoTransacao;
use App\Model\ProdutoSku;
use App\Model\Token;
use App\Model\Usuario;
use DB;

class ProdutoController extends Controller
{
    public function lista(){

        $produto = ProdutoSku::select('id_produto','sku')->orderBy('sku')->get();

        return response()->json($produto, 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);

    }

    public function adicionarProdutos(Request $request)
    {
    	$input = $request->input();

    	//verifica existência dos parâmetros necessários
    	$params_entrada = ["token","nome","descricao","codigo_barras","categorias","sku","quantidade","preco_unitario_compra","preco_sugerido_venda"];
    	$validaParams = Tricks::validacaoEntrada($input,$params_entrada);
    	if($validaParams['validate'] == false){

    		return response()->json($validaParams, 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
    	}

    	//verifica se token é válido
    	$verificaToken = Token::where('hash_token', $input['token'])->first();
    	if(!$verificaToken){
    		return response()->json([
				"message" => "Token Inválido",
				"validate" => false
			], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
    	}

    	//verifica se usuário está ativo
    	$verificaUsuario = Usuario::where('id', $verificaToken->id_usuario)->where('ativo', true)->first();
    	if(!$verificaUsuario){
    		return response()->json([
				"message" => "Usuário está inativo",
				"validate" => false
			], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
    	}

    	//verifica se sku já existe
    	$verificaSku = ProdutoSku::where('sku', $input['sku'])->first();
    	if($verificaSku){
    		return response()->json([
				"message" => "SKU já tem registro. Altere ou adicione itens no estoque.",
				"validate" => false
			], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
    	}

    	Produto::insert(['nome' => $input['nome'], 'codigo_barras' => $input['codigo_barras'], 'descricao' => $input['descricao'], 'created_at' => date('Y-m-d h:i:s')]);
    	$produtoUltimoId = DB::getPdo()->lastInsertId();

    	ProdutoSku::insert(['id_produto' => $produtoUltimoId, 'sku' => $input['sku'], 'created_at' => date('Y-m-d h:i:s')]);
    	$produtoSkuUltimoId = DB::getPdo()->lastInsertId();

    	ProdutoEstoque::insert(['id_produto_sku' => $produtoSkuUltimoId, 'preco_unitario_compra' => $input['preco_unitario_compra'], 'data' => date('Y-m-d h:i:s'), 'created_at' => date('Y-m-d h:i:s')]);
    	$produtoEstoqueUltimoId = DB::getPdo()->lastInsertId();

    	ProdutoPreco::insert(['id_produto' => $produtoUltimoId, 'preco' => $input['preco_sugerido_venda'], 'data_preco_inicio' => date('Y-m-d h:i:s'), 'created_at' => date('Y-m-d h:i:s')]);

    	if(count($input['categorias']) > 0){

    		foreach ($input['categorias'] as $key => $value) {
    			ProdutoCategoria::insert(['id_produto' => $produtoUltimoId, 'id_categoria' => $value, 'created_at' => date('Y-m-d h:i:s')]);
    		}

    	}

    	ProdutoTransacao::insert(['id_produto_estoque' => $produtoEstoqueUltimoId, 'tipo_transacao' => 'adicionado', 'tipo_usuario' => 'api', 'quantidade' => $input['quantidade'], 'created_at' => date('Y-m-d h:i:s')]);

        return response()->json([
				"message" => "Produto inserido com sucesso.",
				"validate" => true
			], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
    }

    public function removerProdutos(Request $request)
    {
    	$input = $request->input();

    	//verifica existência dos parâmetros necessários
    	$params_entrada = ["token","sku","quantidade","preco_unitario","id_cliente"];
    	$validaParams = Tricks::validacaoEntrada($input,$params_entrada);
    	if($validaParams['validate'] == false){

    		return response()->json($validaParams, 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
    	}

    	//verifica se token é válido
    	$verificaToken = Token::where('hash_token', $input['token'])->first();
    	if(!$verificaToken){
    		return response()->json([
				"message" => "Token Inválido",
				"validate" => false
			], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
    	}

    	//verifica se sku existe
    	$verificaProdutoSku = ProdutoSku::where('sku', $input['sku'])->first();
    	if(!$verificaProdutoSku){
    		return response()->json([
				"message" => "SKU não está registrado",
				"validate" => false
			], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
    	}

    	//verifica se cliente existe
    	$verificaCliente = Cliente::where('id', $input['id_cliente'])->first();
    	if(!$verificaCliente){
    		return response()->json([
				"message" => "Cliente não existe",
				"validate" => false
			], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
    	}

    	//verifica se quantidade é suficiente
    	$produtoEstoque = new ProdutoEstoque();
    	$quantidadeEstoque = $produtoEstoque->selecionaQuantidadeEstoque($verificaProdutoSku->id);

    	if($input['quantidade'] > $quantidadeEstoque){
    		return response()->json([
				"message" => "Quantidade de estoque insuficiente. Quantidade no estoque é $quantidadeEstoque",
				"validate" => false
			], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
    	}

    	//'remover' quantidade de produtos
        $verificaProdutoEstoque = ProdutoEstoque::where('id_produto_sku', $verificaProdutoSku->id)->first();
        if(!$verificaProdutoEstoque){
            return response()->json([
                "message" => "Produto não está registrado no estoque",
                "validate" => false
            ], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
        }


        ProdutoTransacao::insert(['id_produto_estoque' => $verificaProdutoEstoque->id, 'tipo_transacao' => 'removido', 'tipo_usuario' => 'api', 'quantidade' => $input['quantidade'], 'created_at' => date('Y-m-d h:i:s')]);

    	return response()->json([
				"message" => "Produto removido com sucesso.",
				"validate" => true
			], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);


    }

    public function relatorio(Request $request){

        $input = $request->input();

        //verifica existência dos parâmetros necessários
        $params_entrada = ["token"];
        $validaParams = Tricks::validacaoEntrada($input,$params_entrada);
        if($validaParams['validate'] == false){

            return response()->json($validaParams, 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
        }

        //verifica se token é válido
        $verificaToken = Token::where('hash_token', $input['token'])->first();
        if(!$verificaToken){
            return response()->json([
                "message" => "Token Inválido",
                "validate" => false
            ], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);
        }

        $data = isset($input['data']) ? $input['data'] : null;

        if($data == null){
            $data = date('Y-m-d');
        }

        $produtoEstoque = new ProdutoEstoque();
        $produtoEstoqueAdicionado = $produtoEstoque->selecionaEstoquePorDia($data, 'adicionado');
        $produtoEstoqueRemovido = $produtoEstoque->selecionaEstoquePorDia($data, 'removido');
        $produtoEstoqueAlerta = $produtoEstoque->selecionaEstoqueAlerta(100);

        $dados = [
            "dia_referencia" => $data,
            "total_produtos_adicionados" => $produtoEstoqueAdicionado ? count($produtoEstoqueAdicionado) : 0,
            "total_produtos_removidos" => $produtoEstoqueRemovido ? count($produtoEstoqueRemovido) : 0,
            "produtos_adicionados" => $produtoEstoqueAdicionado ? $produtoEstoqueAdicionado : [],
            "produtos_removidos" => $produtoEstoqueRemovido ? $produtoEstoqueRemovido : [],
            "aviso_estoque" => $produtoEstoqueAlerta ? $produtoEstoqueAlerta : []
        ];

        return response()->json([
                "message" => "Relatorio simplicado do dia.",
                "dados" => $dados,
                "validate" => true
            ], 200, Tricks::headerJson(), JSON_UNESCAPED_UNICODE);

    }



}
