<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Usuario extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    protected $table = 'usuario';
    protected $fillable = [ 
    	'id',
		'login',
		'senha',
		'ativo',
		'created_at',
		'updated_at',
		'deleted_at',
	];

    protected $primaryKey = 'id';

    public function selecionaSimple(){

        $results = DB::table($this->table)->get();

        return $results;
    }

}
