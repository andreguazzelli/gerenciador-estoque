<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProdutoCategoria extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    protected $table = 'produto_categoria';
    protected $fillable = [ 
    	'id',
		'id_produto',
		'id_categoria',
		'created_at',
		'updated_at',
		'deleted_at',
	];

    protected $primaryKey = 'id';

    public function selecionaSimple(){

        $results = DB::table($this->table)->get();

        return $results;
    }
}
