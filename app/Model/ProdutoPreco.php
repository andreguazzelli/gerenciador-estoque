<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProdutoPreco extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    protected $table = 'produto_preco';
    protected $fillable = [ 
    	'id',
		'id_produto',
		'preco',
		'data_preco_inicio',
		'data_preco_fim',
		'created_at',
		'updated_at',
		'deleted_at',
	];

    protected $primaryKey = 'id';

    public function selecionaSimple(){

        $results = DB::table($this->table)->get();

        return $results;
    }
}
