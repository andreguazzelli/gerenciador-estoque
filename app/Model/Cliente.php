<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Cliente extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    protected $table = 'cliente';
    protected $fillable = [ 
    	'id',
		'nome',
		'cpf',
		'created_at',
		'updated_at',
		'deleted_at',
	];

    protected $primaryKey = 'id';

    public function selecionaSimple(){

        $results = DB::table($this->table)->get();

        return $results;
    }
}
