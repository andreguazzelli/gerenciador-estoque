<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Categoria extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    protected $table = 'categoria';
    protected $fillable = [ 
    	'id',
		'nome',
		'descricao',
		'created_at',
		'updated_at',
		'deleted_at',
	];

    protected $primaryKey = 'id';

    public function selecionaSimple(){

        $results = DB::table($this->table)->get();

        return $results;
    }
}
