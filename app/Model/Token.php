<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Token extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    protected $table = 'token';
    protected $fillable = [ 
    	'id',
		'hash_token',
		'id_usuario',
		'created_at',
		'updated_at',
		'deleted_at',
	];

    protected $primaryKey = 'id';

    public function selecionaSimple(){

        $results = DB::table($this->table)->get();

        return $results;
    }
}
