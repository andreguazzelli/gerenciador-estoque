<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProdutoTransacao extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    protected $table = 'produto_transacao';
    protected $fillable = [ 
    	'id',
		'id_produto_estoque',
		'tipo_transacao',
		'tipo_usuario',
		'id_cliente',
        'quantidade',
		'created_at',
		'updated_at',
		'deleted_at',
	];

    protected $primaryKey = 'id';

    public function selecionaSimple(){

        $results = DB::table($this->table)->get();

        return $results;
    }
}
