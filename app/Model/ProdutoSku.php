<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProdutoSku extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    protected $table = 'produto_sku';
    protected $fillable = [ 
    	'id',
		'id_produto',
		'sku',
		'created_at',
		'updated_at',
		'deleted_at',
	];

    protected $primaryKey = 'id';

    public function selecionaSimple(){

        $results = DB::table($this->table)->get();

        return $results;
    }
}
