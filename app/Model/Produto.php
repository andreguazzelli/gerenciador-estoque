<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Produto extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    protected $table = 'produto';
    protected $fillable = [ 
    	'id',
		'nome',
		'codigo_barras',
		'descricao',
		'created_at',
		'updated_at',
		'deleted_at',
	];

    protected $primaryKey = 'id';

    public function selecionaSimple(){

        $results = DB::table($this->table)->get();

        return $results;
    }
}
