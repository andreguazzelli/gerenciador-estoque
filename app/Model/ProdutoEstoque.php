<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProdutoEstoque extends Model
{
    public $timestamps = false;
    public $incrementing = false;

    protected $table = 'produto_estoque';
    protected $fillable = [ 
    	'id',
		'id_produto_sku',
		'preco_unitario_compra',
		'data',
		'created_at',
		'updated_at',
		'deleted_at',
	];

    protected $primaryKey = 'id';

    public function selecionaSimple(){

        $results = DB::table($this->table)->get();

        return $results;
    }

    public function selecionaQuantidadeEstoque($id_sku){

        $quantidade = 0;
        /*
            SELECT SUM(pt.quantidade) AS total_adicionado FROM produto_transacao pt
            LEFT JOIN produto_estoque pe ON pt.id_produto_estoque = pe.id
            WHERE pe.id_produto_sku = 1 AND pt.tipo_transacao = 'adicionado'
        */

        $adicionado = DB::table('produto_transacao')
                            ->select(DB::raw('SUM(produto_transacao.quantidade) AS total'))
                            ->leftJoin('produto_estoque', 'produto_estoque.id', '=', 'produto_transacao.id_produto_estoque')
                            ->where('produto_estoque.id_produto_sku',$id_sku)
                            ->where('produto_transacao.tipo_transacao','adicionado')
                            ->get();

        if($adicionado){
            $quantidade += $adicionado[0]->total;
        }

        $removido = DB::table('produto_transacao')
                            ->select(DB::raw('SUM(produto_transacao.quantidade) AS total'))
                            ->leftJoin('produto_estoque', 'produto_estoque.id', '=', 'produto_transacao.id_produto_estoque')
                            ->where('produto_estoque.id_produto_sku',$id_sku)
                            ->where('produto_transacao.tipo_transacao','removido')
                            ->get();

        if($removido){
            $quantidade -= $removido[0]->total;
        }

        return $quantidade;
    }

    public function selecionaEstoquePorDia($data,$tipo_transacao){
        /*
            SELECT ps.id AS id_produto_sku, p.nome AS nome_produto, SUM(pt.quantidade) AS produto_quantidade, ps.sku FROM produto_transacao pt
            LEFT JOIN produto_estoque pe ON pt.id_produto_estoque = pe.id
            LEFT JOIN produto_sku ps ON ps.id = pe.id_produto_sku
            LEFT JOIN produto p ON p.id = ps.id_produto
            WHERE pt.tipo_transacao = 'adicionado'
            AND pt.created_at >= '2020-04-20 00:00:00' AND pt.created_at <= '2020-04-20 23:59:59'
            GROUP BY sku
        */

        $todos_dia = DB::table('produto_transacao')
                            ->select('produto_sku.sku', 'produto.nome AS nome_produto', DB::raw('SUM(produto_transacao.quantidade) AS total'), 'produto_transacao.tipo_usuario AS transacao_via' )
                            ->leftJoin('produto_estoque', 'produto_estoque.id', '=', 'produto_transacao.id_produto_estoque')
                            ->leftJoin('produto_sku', 'produto_sku.id', '=', 'produto_estoque.id_produto_sku')
                            ->leftJoin('produto', 'produto.id', '=', 'produto_sku.id_produto')
                            ->where('produto_transacao.tipo_transacao',$tipo_transacao)
                            ->where('produto_transacao.created_at','>=',$data.' 00:00:00')
                            ->where('produto_transacao.created_at','<=', $data.' 23:59:59')
                            ->groupBy('produto_sku.sku', 'produto_transacao.tipo_usuario')
                            ->get();

        return  $todos_dia;

    }

    public function selecionaEstoqueAlerta($valor_alerta){

        $query = "SELECT * FROM (SELECT adicionados.nome_produto, IF(adicionados.produto_quantidade, adicionados.produto_quantidade, 0) AS adicionados_total, 
                IF(removidos.produto_quantidade, removidos.produto_quantidade, 0) AS removidos_total,
                IF(adicionados.produto_quantidade, adicionados.produto_quantidade, 0) - IF(removidos.produto_quantidade, removidos.produto_quantidade, 0) AS total 
                FROM produto_sku
                LEFT JOIN 
                (select produto.nome AS nome_produto, 
                SUM(produto_transacao.quantidade) AS produto_quantidade, 
                produto_sku.sku from `produto_transacao` 
                left join `produto_estoque` on `produto_estoque`.`id` = `produto_transacao`.`id_produto_estoque` 
                left join `produto_sku` on `produto_sku`.`id` = `produto_estoque`.`id_produto_sku` 
                left join `produto` on `produto`.`id` = `produto_sku`.`id_produto` 
                where `produto_transacao`.`tipo_transacao` = 'adicionado'
                GROUP BY sku) AS adicionados ON produto_sku.sku = adicionados.sku
                LEFT JOIN 
                (select produto.nome AS nome_produto, 
                SUM(produto_transacao.quantidade) AS produto_quantidade, 
                produto_sku.sku from `produto_transacao` 
                left join `produto_estoque` on `produto_estoque`.`id` = `produto_transacao`.`id_produto_estoque` 
                left join `produto_sku` on `produto_sku`.`id` = `produto_estoque`.`id_produto_sku` 
                left join `produto` on `produto`.`id` = `produto_sku`.`id_produto` 
                where `produto_transacao`.`tipo_transacao` = 'removido'
                GROUP BY sku) AS removidos ON produto_sku.sku = removidos.sku) AS tudo
                WHERE tudo.total < {$valor_alerta}";

        $alerta = DB::select($query);

        return  $alerta;

    }
}
